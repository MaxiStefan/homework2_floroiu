function addTokens(input, tokens) {


    if (typeof input !== 'string') {
        throw new Error('Invalid input')
    }


    if (input.length < 6) {
        throw new Error("Input should have at least 6 characters ")
    }

    let newString = input

    const arrayFormat = tokens.every(x => typeof x === `string`)
    if (arrayFormat === false) {
        throw new Error('Invalid array format')
    }


    if (newString.indexOf("...") !== -1) {
        let vector = newString.split("...")
        vector.splice(-1)
        for (var i = 0; i < vector.length; i++) {
            vector[i] += "${" + tokens[i].tokenName + "}"
        }
        let finalVector = vector.join()

        // for ( let key in tokens){
        //     if (newString.indexOf("...")!==-1){
        //         let token = "${" + tokens[key].tokenName + "}"
        //         newString = newString.replace("...", token)
        //     }
        // }


    }
    // else {
    //     return newString
    // }
    return finalVector.toString()
}

// let str = "Hey there ..., how are you ..."
// tokens = [{tokenName: "Max"},{tokenName: "today"}]
// let vector = str.split("...")
// vector.splice(-1)
// for( var i =0; i< vector.length; i++)
//     vector[i] += "${"+tokens[i].tokenName+"}"
//     console.log(vector)
//     let finalVector=vector.join()


// console.log(finalVector.toString())



const app = {
    addTokens: addTokens
}

module.exports = app;